## xTract
### Targeted extraction of ion chromatograms.

The software xTract[1] has been designed to extract ion chromatograms of cross-linked peptides using the xQuest/xProphet[2-4] pipeline. It uses a unique target - decoy strategy to provide statistics on the extracted ion chromatograms. For this purpose, the software mProphet[5] is used.

![image](https://gitlab.ethz.ch/leitner_lab/xtract/-/wikis/uploads/bf24fb00f31321b6d12e277e06dbd3a4/image.png)
xTract example chromatogram: Shown are the extracted isotopes and the summed isotopes (cyan).

### xTract Repository Wiki
More information can be found on the various pages of the xTract repository wiki:
* [Home](https://gitlab.ethz.ch/leitner_lab/xtract/-/wikis/home)
* [Disclaimer](https://gitlab.ethz.ch/leitner_lab/xtract/-/wikis/Disclaimer)
* [Download](https://gitlab.ethz.ch/leitner_lab/xtract/-/wikis/Download)
* [Installation](https://gitlab.ethz.ch/leitner_lab/xtract/-/wikis/Installation)
* [How to run](https://gitlab.ethz.ch/leitner_lab/xtract/-/wikis/How-to-run)


### Software components
The software xTract consists of multiple programs that are executed consequtively.
All necessary programs are located in the /bin directory and have a help section (execute the program with the -h flag). 
* xtract_add_precursor_intensity.pl, extracts the precursor intensities from the MS2 headers and adds it to the input ID file.
* xtract_prep.pl, prepares the folderstructures for the ion chromatogram extraction.
* xtract_decoys.pl, creates decoy peptides based on the target IDs.
* xtract_run.pl, executes the xtract.pl program.
* xtract_merger.pl, merges the results and runs the statistical analysis of peak groups by mProphet[4].
* xtract_normalizer.pl, generates normalization parameters using common identified peptides.
* xtract_analyzer.pl, analyzes the extraction results and provides statistics.

### References
1. Walzthoeni T, Joachimiak LA, Rosenberger G, Röst HL, Malmström L, Leitner A, Frydman R, Aebersold R (2015) xTract: software for characterizing conformational changes of protein complexes by quantitative cross-linking mass spectrometry. [Nature methods.](http://dx.doi.org/10.1038/nmeth.3631)
2. Walzthoeni T, Claassen M, Leitner A, Herzog F, Bohn S, Förster F, Beck M, Aebersold R (2012) False discovery rate estimation for cross-linked peptides identified by mass spectrometry. [Nature methods.](http://dx.doi.org/10.1038/nmeth.2103)
3. Rinner O, Seebacher J, Walzthoeni T, Mueller L, Beck M, Schmidt A, Mueller M, Aebersold R (2008) Identification of cross-linked peptides from large sequence databases. [Nature methods.](http://www.nature.com/nmeth/journal/v5/n4/abs/nmeth.1192.html)
4. Leitner A, Walzthoeni T, Aebersold R. (2014) Lysine-specific chemical cross-linking of protein complexes and identification of cross-linking sites using LC-MS/MS and the xQuest/xProphet software pipeline. [Nature protocols.](http://dx.doi.org/10.1038/nprot.2013.168)
5. Reiter et al. (2011) mProphet: automated data processing and statistical validation for large-scale SRM experiments. [Nature methods.](http://dx.doi.org/10.1038/nmeth.1584)
