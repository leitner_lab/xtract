Changelog xTract version 1.0.2 06/2017
- Sorting of output files and rounding of scores to 6 decimal places. 
- Added the set.seed() function to mProphet.